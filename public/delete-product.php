<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__.'/../vendor/autoload.php';

$id = $_POST['id'] ?? null;
if (!$id) {
	exit;
}

$repo = new \App\Repositories\ProductRepository;

try {
	$repo->delete ($id);
	$response = ['message' => 'Product successfully deleted!'];
	$code = 200;

} catch (Exception $error) {
	$response = ['message' => $error->getMessage ()];
	$code = 400;
}

echo (json_encode ($response));

header('Content-Type: application/json');
http_response_code($code);
?>