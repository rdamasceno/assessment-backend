<?php
namespace App\Models;

class CategoryModel extends Model {

	protected $id;

	protected $code;

	protected $name;

	/**
	 * @return string
	 */
	function getTable () {
		return 'categories';
	}

	/**
	 * @return array
	 */
	function getFillable () {
		return ['code', 'name'];
	}

	/**
	 * @return mixed
	 */
	public function getId () {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId ($id) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getCode () {
		return $this->code;
	}

	/**
	 * @param mixed $code
	 */
	public function setCode ($code) {
		$this->code = $code;
	}

	/**
	 * @return mixed
	 */
	public function getName () {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName ($name) {
		$this->name = $name;
	}
}