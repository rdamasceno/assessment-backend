<?php
namespace App\Services;

class Utils {

	static function camelize ($string) {
		$string = preg_replace ('/(_|-)+/', ' ', $string);
		$string = ucwords ($string);
		$string[0] = strtolower ($string[0]);

		return $string;
	}
}