<?php
namespace App\Services\File\Validators;

class ImageValidator implements IValidator {

	private $mimeTypes = [
		'image/jpeg',
		'image/jpg',
		'image/png',
		'image/gif'
	];
	/**
	 * {@inheritdoc}
	 */
	function validate ($file, ...$args) {

		$mime_type = $file['type']??null;
		if ($mime_type && !in_array ($mime_type, $this->mimeTypes)) {
			return "The uploaded file must be an image";
		}

		return null;
	}
}