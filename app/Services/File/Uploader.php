<?php
namespace App\Services\File;

class Uploader {

	/**
	 * Lista dos validadores de arquivo disponíveis.
	 *
	 * @var Validators\IValidator[] $validators
	*/
	protected $validators = [];

	/**
	 * @var string $rootDir
	*/
	protected $rootDir;

	/**
	 * Uploader constructor.
	 */
	public function __construct () {

		$this->rootDir    = realpath (__DIR__.'/../../..' );
		$this->validators = [
			'image'    => new Validators\ImageValidator(),
			'maxSize'  => new Validators\MaxSizeValidator()
		];
	}

	/**
	 * Valida o arquivo de acordo com as regras fornecidas.
	 *
	 * @param $file
	 * @param $rules
	 * @throws \Exception
	 */
	function validate ($file, string $rules) {

		$errors = [];

		if (is_string ($rules)) {
			$rules = explode ('|', $rules);
		}

		foreach ($rules as $rule) {
			$pieces    = explode (':', $rule);
			$validator = array_shift ($pieces);

			if (isset($this->validators[$validator])) {
				$args = count($pieces) ? explode (',', $pieces[0]) : [];
				if ($error = $this->validators[$validator]->validate ($file, $args)) {
					$errors[] = $error;
				}
			}
		}

		if (!empty($errors)) {
			throw new \Exception("Invalid file sent: ".implode ('; ', $errors), 400 );
		}
	}

	/**
	 * Efetuar o upload da arquivo fornecido no diretório indicado.
	 *
	 * @param $file
	 * @param $path
	 * @return string
	 * @throws \Exception
	 */
	function upload ($file, $path = '') {

		$path = $this->rootDir.'/public/assets/images'.($path ? "$path/" : '');
		if (!is_dir ($path)) {
			mkdir ($path, 755);
		}
		$filename = $path.basename($file['name']);
		$file_tmp = $file['tmp_name'];

		if (!(move_uploaded_file($file_tmp, $filename))) {
			throw new \Exception("Ops! Não foi possível transferir o arquivo {$file['name']}.");
		}

		return str_replace ($this->rootDir. '/public', '', $filename);
	}

	/**
	 * Remove o arquivo fornecido.
	 *
	 * @param $filename
	 * @return bool
	 */
	function delete ($filename) {
		return unlink ($this->rootDir.'/public/'.$filename);
	}
}