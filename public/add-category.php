<?php
require '../app/Views/Header.php';

$category_repo 	= new \App\Repositories\CategoryRepository();
$category    	= new \App\Models\CategoryModel();

$id    			= $_GET['id']??null;
$message		= [];
$validation_errors = [];

if (!empty($id)) {
	try {
		$category = $category_repo->findOrFail ($id);

	} catch (\Exception $exception) {
		echo '<script type="text/javascript">
				alert("'.$exception->getMessage ().'"); 
				window.location="/categories.php";
			</script>';
	}

}

if (!empty($_POST)) {

	$attributes = [
		'code' 	=> "{$_POST['code']}"??null,
		'name' 	=> "{$_POST['name']}"??null
	];

	try {
	    $category->fill ($attributes);
		if ($category->exists ()) {
			$category = $category_repo->update ($category->getKey (), $attributes);

		} else {
			$category_repo->store ($attributes);
			$category = new \App\Models\CategoryModel();
		}

		$message = [
			'type' 	  => 'success',
			'content' => 'Categoria salva com sucesso!'
		];

	} catch (\App\Repositories\Concerns\ValidationException $exception) {
		$validation_errors = $exception->getErrors ();

	} catch (Exception $exception) {
		$message = [
			'type' 	  => 'danger',
			'content' => $exception->getMessage (),
		];
	}
}
?>

<main class="content">
	<h1 class="title new-item"><?php echo $category->exists () ? 'Editar Categoria' : 'Nova Categoria'; ?></h1>

	<?php if (!empty($message)): ?>
		<div class="alert alert-<?php echo $message['type'];?>">
			<small><?php echo $message['content']; ?></small>
		</div>
	<?php endif; ?>

	<form method="post">
		<div class="input-field">
			<label for="category-name" class="label">Category Name</label>
			<input type="text" id="category-name" class="input-text" name="name" value="<?php echo $category->getName ()?>" />
			<?php if(isset($validation_errors['name'])): ?>
				<span class="input-info input-info-danger">
					<?php echo implode ('; ', $validation_errors['name']); ?>
				</span>
			<?php endif; ?>

		</div>
		<div class="input-field">
			<label for="category-code" class="label">Category Code</label>
			<input type="text" id="category-code" class="input-text" name="code" value="<?php echo $category->getCode ()?>" />
			<?php if(isset($validation_errors['code'])): ?>
				<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['code']); ?>
					</span>
			<?php endif; ?>
		</div>
		<div class="actions-form">
			<a href="categories.php" class="action back">Back</a>
			<input class="btn-submit btn-action"  type="submit" value="Save" />
		</div>
	</form>
</main>

<?php require '../app/Views/Footer.php'; ?>