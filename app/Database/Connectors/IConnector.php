<?php

namespace App\Database\Connectors;

interface IConnector {

	/**
	 * @param array $config
	 * @return \PDO
	 */
	function connect (array $config = []);
}