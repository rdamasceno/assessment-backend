<?php
namespace App\Database\Query;

class MysqlBuilder extends Builder {

	private function normalizeQueryValue ($value) {

		if (!is_numeric ($value) && is_string ($value)) {
			$value = preg_replace ('/(\'|\")/', '', $value);
			$value = "'$value'";
		}

		if (is_null ($value)) {
			$value = 'NULL';
		}

		return $value;
	}

	/**
	 * {@inheritdoc}
	 */
	function where ($column, $operator = null, $value = null, $connector = 'and') {

		if (is_callable ($column)) {
			call_user_func($column, $query = new MysqlBuilder($this->connection));
			$clause = '('. preg_replace  ('/^(and|or)\s+/', '', implode (' ', $query->getWheres () )) . ')';

		} else {
			$clause = "$column $operator {$this->normalizeQueryValue($value)}";
		}

		$this->wheres[] = "$connector $clause";

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	function orderBy ($column, $direction = 'ASC') {

		if (is_array ($column)) {
			$this->orders = array_merge ($this->orders, $column);

		} else {
			$this->orders[] = "$column $direction";
		}

		return $this;
	}

	/**
	 * @return null|string
	 */
	private function makeWhereClause () {

		if (!empty($this->wheres)) {
			$text = implode (' ', $this->wheres);
			$text = preg_replace ('/^(or|and)\s+/', '', $text);

			return "WHERE $text";
		}

		return null;
	}

	/**
	 * @return null|string
	 */
	private function makeOrderClause () {

		if (!empty($this->orders)) {
			return 'ORDER BY '.implode (", ", $this->orders);
		}

		return null;
	}

	/**
	 * @return string
	 */
	private function getSelectSQL () {

		$columns = '*';

		if (!empty($this->columns)) {
			$columns = implode (',', array_map (function($column){ return "`$column`"; }, $this->columns));
		}

		$sql = "SELECT $columns FROM `{$this->from}`";

		if ($whereClause = $this->makeWhereClause()) {
			$sql.= " $whereClause";
		}

		if ($orderClause = $this->makeOrderClause()) {
			$sql.= " $orderClause";
		}

		return $sql;
	}

	/**
	 * {@inheritdoc}
	 */
	function selectOne () {
		$result = $this->connection->selectOne ($this->getSelectSQL ());

		if (!empty($result) && is_callable ($this->transformer)) {
			$result = call_user_func ($this->transformer, $result);
		}

		$this->clear();
		return $result;
	}

	/**
	 * {@inheritdoc}
	 */
	function selectAll () {


		$results = $this->connection->selectAll ($this->getSelectSQL ());

		if (!empty($results) && is_callable ($this->transformer)) {
			$results = array_map (function($result){
				return call_user_func ($this->transformer, $result);
			}, $results);
		}

		$this->clear();
		return $results;
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	function create ($attributes = []) {

		$columns = implode (',', array_keys ($attributes));
		$values  = array_map (function($value){
			return $this->normalizeQueryValue ($value);
		}, array_values ($attributes));
		$values  = implode (',', array_values ($values));

		$sql = "INSERT INTO `{$this->from}` ($columns) VALUES ($values)";

		$this->clear();
		$this->connection->execute ($sql);
		return $this->connection->getLastInsertId ();
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	function update ($attributes = []) {

		$sql    = "UPDATE `{$this->from}` SET ";
		$fields = array_map (function($key, $value){
			return "$key={$this->normalizeQueryValue ($value)}";
		}, array_keys ($attributes), array_values ($attributes));

		$sql.= implode (', ', $fields);

		if ($whereClause = $this->makeWhereClause()) {
			$sql.= " $whereClause";
		}

		$this->clear();
		$this->connection->execute ($sql);
		return true;
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	function delete () {

		$sql = "DELETE FROM `{$this->from}`";

		if ($whereClause = $this->makeWhereClause()) {
			$sql.= " $whereClause";
		}

		$this->clear();
		$this->connection->execute ($sql);
		return true;
	}
}