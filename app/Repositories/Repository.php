<?php
namespace App\Repositories;

use App\Models\Model;

abstract class Repository {

	use Concerns\HasValidations;

	protected $model;

	/**
	 * @return \App\Models\Model
	 */
	protected function newModel () {
		return new $this->model;
	}

	/**
	 * @param array $attributes
	 * @param string $method
	 */
	protected function validateAttributes (array $attributes, string $method) {}

	/**
	 * @param \App\Database\Query\Builder $query
	 * @param $queryOptions
	 * @return \App\Database\Query\Builder
	 */
	protected function withSelectAllQuery ($query, $queryOptions) {
		return $query;
	}

	/**
	 * @param array $queryOptions
	 * @return array
	 */
	function find (array $queryOptions = []): array {
		return $this->withSelectAllQuery ($this->newModel ()->newQueryBuilder(), $queryOptions)->selectAll ();
	}

	/**
	 * @param int $id
	 * @return Model|null
	 */
	function findOne ($id) {
		return $this->newModel ()->find($id);
	}

	/**
	 * @param int $id
	 * @return Model
	 * @throws \Exception
	 */
	function findOrFail ($id): Model {
		$model = $this->findOne ($id);
		if (!$model) {
			throw new \Exception("Item with id '$id' not found");
		}

		return $model;
	}

	/**
	 * @param array $attributes
	 * @return Model
	 */
	function store (array $attributes): Model {
		$this->validateAttributes ($attributes, 'store');
		return $this->newModel ()->create($attributes);
	}

	/**
	 * @param $id
	 * @param array $attributes
	 * @return Model
	 * @throws \Exception
	 */
	function update ($id, array $attributes = []): Model {
		$model = $this->findOrFail ($id);
		$model->fill ($attributes);

		$this->validateAttributes ($model->toArray (), 'update');
		$model->update ();

		return $model;
	}

	/**
	 * @param $id
	 * @return bool
	 * @throws \Exception
	 */
	function delete ($id): bool {
		$model = $this->findOrFail ($id);
		$model->delete ();

		return true;
	}
}