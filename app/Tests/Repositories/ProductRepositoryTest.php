<?php
namespace App\Repositories;

use App\Database\Connection;
use App\Models\ProductModel;

class ProductRepositoryTest extends \PHPUnit\Framework\TestCase {

	/**
	 * Clear the product
	 * @throws \Exception
	 */
	function __destruct () {
		Connection::getInstance ('test')->execute ("DELETE FROM `products`");
	}

	function testStore () {

		ProductModel::testing (function(){

			$repo 	= new ProductRepository();
			$model1 = $repo->store ([
				'sku'		  => 'test-12345-01',
				'name' 		  => 'Product 1',
				'description' => 'Product\'s description',
				'price' 	  => '100.00',
				'quantity'    => '20',
			]);

			$model2 = $repo->store ([
				'sku'		  => 'test-12345-02',
				'name' 		  => 'Product 2',
				'description' => 'Product\'s description',
				'price' 	  => '100.00',
				'quantity'    => '20',
			]);

			$this->assertTrue ($model1 instanceof \App\Models\ProductModel);
			$this->assertTrue ($model2 instanceof \App\Models\ProductModel);
		});
	}

	function testFind () {

		ProductModel::testing (function() {

			$repo = new ProductRepository();
			$a = $repo->find ();
			$b = $repo->find (['sku' => 'test-12345-01']);
			$c = $repo->find (['sku' => 'test-12345-0x']);

			$this->assertCount (2, $a);
			$this->assertCount (1, $b);
			$this->assertCount (0, $c);
		});
	}

	function testFindOne () {
		ProductModel::testing (function() {

			$repo = new ProductRepository();
			$model = $repo->findBySku ('test-12345-01');

			$this->assertSame ( 'test-12345-01', $model->getSku ());
		});
	}

	function testUpdate () {
		ProductModel::testing (function() {

			$repo  = new ProductRepository();
			$model = $repo->findBySku ('test-12345-01');

			if ($model) {
				$model = $repo->update ($model->getKey (), ['name' => 'new product name']);
				$this->assertSame ( 'new product name', $model->getName ());
			}
		});
	}

	function testDelete () {
		ProductModel::testing (function() {

			$repo  = new ProductRepository();
			$model = $repo->findBySku ('test-12345-01');

			if ($model) {
				$this->assertTrue ( $repo->delete ($model->getKey ()));
			}
			$this->expectExceptionMessage ("Item with id '{$model->getKey ()}' not found");
			$repo->delete ($model->getKey ());
		});
	}
}